# SteplixDocker

En este repositorio se encuentran absolutamente todas las `guias` de la empresa `Steplix` para crear `docker images`, paso a paso, para los `proyectos` que requieren `continuos integration` en `gitlab`.


## Bash Client

Muchas de estas guias utilizan `steplix command line client` para `Ubuntu 16.04`. La documentación de la instalación y el uso se encuentran en la sección [Wiki del proyecto SteplixClients](https://gitlab.com/steplix/SteplixClients/-/wikis/Bash-Client).

## Docker para Android

En la [siguiente guía](https://gitlab.com/steplix/SteplixDocker/-/wikis/Android-Docker-Image) ubicada en la [sección wiki](https://gitlab.com/steplix/SteplixDocker/-/wikis/home) de este proyecto, se encuentra el paso a paso para crear una `imagen` de `docker` para compilar aplicaciones `android` y utilizar `gradlew` para ejecutar tasks.

## Publicar una imagen en el Cloud

En la [siguiente guía](https://gitlab.com/steplix/SteplixDocker/-/wikis/Publishing-to-Gitlab) ubicada en la [sección wiki](https://gitlab.com/steplix/SteplixDocker/-/wikis/home) de este proyecto, se encuentra el paso a paso para publicar una `imagen` de `docker` en https://hub.docker.com que luego puede ser referenciada desde `.gitlab-ci.yml` al momento de hacer `continuos integration`.